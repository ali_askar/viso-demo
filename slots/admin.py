from django.contrib import admin
from slots.models import Slots


@admin.register(Slots)
class StatusAdmin(admin.ModelAdmin):
    list_display = [

        'snapshot_tag', 'roi1', 'roi2', 'roi3', 'roi4', 'roi5', 'roi6'
    ]

    # @staticmethod
    # def image_preview(obj):
    #     return f'<img src="data:image/jpeg;base64,{obj.image_data}" style="max-width: 100px; max-height: 100px;" />'
