import base64
import time
import uuid
from base64 import b64decode

from django.core.files.base import ContentFile
from django.db import models
from django.utils.safestring import mark_safe


# Create your models here.

class Slots(models.Model):
    class Status(models.TextChoices):
        EMPTY = "EMPTY"
        OCCUPIED = "OCCUPIED"

    roi1 = models.CharField(max_length=10, choices=Status.choices, )

    roi2 = models.CharField(max_length=10, choices=Status.choices)

    roi3 = models.CharField(max_length=10, choices=Status.choices)

    roi4 = models.CharField(max_length=10, choices=Status.choices)

    roi5 = models.CharField(max_length=10, choices=Status.choices)

    roi6 = models.CharField(max_length=10, choices=Status.choices)

    snapshot = models.ImageField(upload_to='snapshots', blank=True)

    def snapshot_tag(self):
        return mark_safe('<img src="/media/%s" width="500" height="300" />' % self.snapshot)

    @staticmethod
    def create_slots_from_payload(payload):
        try:
            result = payload['result']
            # Iterate over each slot in the payload
            slots_instance = Slots(
                roi1=result.get("roi1").get("status").upper(),
                roi2=result.get("roi2").get("status").upper(),
                roi3=result.get("roi3").get("status").upper(),
                roi4=result.get("roi4").get("status").upper(),
                roi5=result.get("roi5").get("status").upper(),
                roi6=result.get("roi6").get("status").upper(),
            )

            # Save the Slots instance
            slots_instance.save()

            # Save the snapshot image
            base64_image_data = payload["snapshot"]
            ff, img_str = base64_image_data.split(';base64,')
            ext = ff.split('/')[-1]
            file_name = f'snapshot_{int(time.time())}_{uuid.uuid4().hex[:8]}.{ext}'
            slots_instance.snapshot.save(file_name, ContentFile(base64.b64decode(img_str)))

        except Exception as e:
            raise e
