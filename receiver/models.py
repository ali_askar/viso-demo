from django.db import models

# Create your models here.

class CarEvent(models.Model):
    data = models.JSONField()
    created_on_datetime = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        date = self.created_on_datetime.strftime("%Y-%m-%d %H:%M:%S")
        return f'event at {date}' 