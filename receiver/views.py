from rest_framework import status

import requests
import simplejson as json
from receiver import models
from slots.models import Slots
from rest_framework.response import Response
from rest_framework.decorators import api_view


@api_view(['POST'])
def receive(request):
    try:
        # data = json.dumps(request.data, indent=4, sort_keys=True)
        # models.CarEvent.objects.create(data=json.loads(data))
        should_save = check_new_requests(request.data)

        if should_save:
            Slots.create_slots_from_payload(request.data)

        send_slack_alert_request(request)

        return Response()

    except Exception as e:
        send_slack_error_request(e)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def check_new_requests(payload):
    result = payload['result']
    slots = Slots.objects.last()
    if not slots:
        return True

    for roi in ['roi1', 'roi2', 'roi3', 'roi4', 'roi5', 'roi6']:
        current_status = result.get(roi).get("status").upper()
        if getattr(slots, roi) != current_status:
            return True

    return False


def send_slack_alert_request(data):
    data.data.pop('snapshot')
    url = "https://hooks.slack.com/services/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"
    text = f"""VISO-DEMO:\n{json.dumps(data.data, indent=4, sort_keys=True)}"""
    data = {"text": text}
    requests.post(url, json=data)


def send_slack_error_request(e):
    url = "https://hooks.slack.com/services/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"
    text = f"""VISO-DEMO:\n{e} """
    data = {"text": text}
    requests.post(url, json=data)
