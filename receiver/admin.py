from django.contrib import admin
from receiver.models import CarEvent


# Register your models here.

@admin.register(CarEvent)
class CarEventAdmin(admin.ModelAdmin):
    pass
